#!/usr/bin/env bash

# Create a Rancher K3s multi-master cluster using multipass VMs

# If you blow out a master node or two you may come across this issue:
# https://github.com/canonical/dqlite/issues/190
# https://github.com/rancher/k3s/issues/1403

# This file uses shellcheck directives: https://github.com/koalaman/shellcheck/wiki/Directive

### Set the variables
  ctrlc_count=0; trap no_ctrlc SIGINT
  # BASENAME=${0##*/}
  set -o allexport
  KUBECONFIG="${HOME}/.kube/k3s.yaml"
  K3S_NODEIP_MASTER=""
  K3S_URL=""
  LEADING_NODE=""
  K3S_NODEIP_WORKER=""
  ZEBRIUMCONFIG="${HOME}/.zebrium/config"
  # ISTIO: DISABLED as per https://istio.io/docs/setup/install/helm/ 
  # Helm 3 is not supported because the chart uses the crd-install hook which was dropped in Helm 3
  NO_DEPLOY_ISTIO="true" 
  set +o allexport

  # Application settings not inclued above
  KUBECTL_INSTALLED=""
  HELM_INSTALLED=""
  INSTALL_K3S_VERSION="v1.17.4+k3s1"
  # INSTALL_K3S_EXEC_WORKER="" #"--no-deploy=traefik --no-deploy=servicelb"
  VERBOSE_LEVEL=0
  ZEBRIUM_NAMESPACE="zebrium"
  ZEBRIUM_POSTFIX="k3s-"$(date +%Y-%m-%d)
  
  # Set node names
  PREFIX="k3s-"
  MASTER_COUNT=1 ; NAME_M=$PREFIX"master0"
  WORKER_COUNT=3 ; NAME_W=$PREFIX"worker0"
  
  # Set the Multipass launch configuration unless overridden by args
  NODELIST=""
  K3S_CPUS_M="1" ; K3S_MEM_M="1G" ; K3S_DISK_M="3G" ; K3S_IMAGE_M="release:20.04"
  K3S_CPUS_W="1" ; K3S_MEM_W="1G" ; K3S_DISK_W="3G" ; K3S_IMAGE_W="release:20.04"
  
   K3S_IMAGE_W="release:20.04"
  
### Variables end

### Source the functions
  # shellcheck source=./functions/helpers.bash
  source ./functions/helpers.bash
  # shellcheck source=./functions/info.bash
  source ./functions/info.bash
  # shellcheck source=./functions/prerequisites.bash
  source ./functions/prerequisites.bash
  # shellcheck source=./functions/release.bash
  source ./functions/release.bash

  # shellcheck source=./functions/config_cloud-init.bash
  source ./functions/config_cloud-init.bash
  # shellcheck source=./functions/config_cloud-init.bash
  source ./functions/config_dashboard.bash
  # shellcheck source=./functions/config_istio.bash
  source ./functions/config_istio.bash
  # shellcheck source=./functions/config_k3s.bash
  source ./functions/config_k3s.bash
  # shellcheck source=./functions/config_kubectl.bash
  source ./functions/config_kubectl.bash
  # shellcheck source=./functions/config_metrics-server.bash
  source ./functions/config_metrics-server.bash
  # shellcheck source=./functions/config_multipass.bash
  source ./functions/config_multipass.bash
  # shellcheck source=./functions/config_nginx.bash
  source ./functions/config_nginx.bash
  # shellcheck source=./functions/config_prometheus.bash
  source ./functions/config_prometheus.bash
  # shellcheck source=./functions/config_weave-scope.bash
  source ./functions/config_weave-scope.bash
  # shellcheck source=./functions/config_zebrium.bash
  source ./functions/config_zebrium.bash
  
### Source end

###################
### main script ###
###################

check_root
### Args ###
if [[ $# -eq 0 ]];
  then
  k3s_help; exit 0
  else
  set -u
  while [[ $# -gt 0 ]]; do
    case $1 in
      -h | '--help' ) k3s_help; exit 0 ;;
      -i | '--info' ) info_block; exit 0 ;;
      -v | '--verbose' ) VERBOSE_LEVEL=1 ;;
      -vv) VERBOSE_LEVEL=2 ;;
      -vvv) VERBOSE_LEVEL=3 ;;

      -d | '--no-deploy' )
        shift
        if [[ $# -ne 0 ]]; then
            case ${1} in
              dashboard)
              export NO_DEPLOY_DASHBOARD="true"
              ;;
              istio)
              export NO_DEPLOY_ISTIO="true"
              ;;
              prometheus)
              export NO_DEPLOY_PROMETHEUS="true"
              ;;
              weavescope)
              export NO_DEPLOY_WEAVESCOPE="true"
              ;;
              *)
              echo "Component ${1} not supported"
              exit 1
              ;;
            esac
          else
          echo -e "Please provide the component name. e.g. -d dashboard or --no-deploy dashboard"
          exit 0
        fi
        ;;
      -r | '--release' )
        shift
        if [[ $# -ne 0 ]]; then
            case ${1} in
            stable)
              installed jq
              set -a; INSTALL_K3S_VERSION=$(curl -s https://api.github.com/repos/rancher/k3s/releases/latest | jq -r '.tag_name'); set +a
              info "Stable K3s version set: $INSTALL_K3S_VERSION"
            ;;
            latest)
              installed jq
              set -a; INSTALL_K3S_VERSION="$(curl -s https://api.github.com/repos/rancher/k3s/releases | jq -r '.[0].tag_name')"; set +a
              info "Latest K3s version set: $INSTALL_K3S_VERSION"
            ;;
            *)
              export INSTALL_K3S_VERSION="${1}"
            ;;
            esac
          else
            echo -e "Please provide the desired version. e.g. --version v1.17.4+k3s1 or -v stable or -v latest"
            exit 0
        fi
        ;;
      -m) shift; MASTER_COUNT=$1 ;;
      -w) shift; WORKER_COUNT=$1 ;;
      
      -mc) shift; export K3S_CPUS_M=$1 ;; 
      -mm) shift; export K3S_MEM_M=$1"G" ;; 
      -md) shift; export K3S_DISK_M=$1"G" ;; 
      -mi | '--master-image' ) shift; export K3S_IMAGE_M=$1 ;;
      
      -wc) shift; export K3S_CPUS_W=$1 ;; 
      -wm) shift; export K3S_MEM_W=$1"G" ;; 
      -wd) shift; export K3S_DISK_W=$1"G" ;; 
      -wi | '--worker-image' ) shift; export K3S_IMAGE_W=$1 ;;
      
      *) exit 1
      ;;
    esac

    shift
  done
  set +u
fi
installed jq
installed kubectl
installed helm
installed multipass
multipass_cleanup
k3s_release_info
node_config
k3s_cloud_init_base
k3s_setup
k3s_labels
kubectl_configuration
# ALL the following installs need to come after the cluster setup
#zebrium_install
dashboard_install
#metrics-server_install # OBSOLETE DUE TO: https://github.com/rancher/k3s/issues/990
#weave-scope_install
#prometheus-operator_install
#istio_install

### Current Errors ###
  # 2020-05-11 09:21:30 agent E  6469 kubelet.go:1298]
  # Image garbage collection failed multiple times in a row:
  # failed to garbage collect required amount of images.
  # Wanted to free 427802624 bytes, but freed 0 bytes

### Future improvements ###
 # --- fail_trap is executed if an error occurs. --- ### NEXT IMPROVEMENT
  # fail_trap {
  #   result=$?
  #   if [ "$result" != "0" ]; then
  #     if [[ -n "$INPUT_ARGUMENTS" ]]; then
  #       echo "Failed to install $BASENAME with the arguments provided: $INPUT_ARGUMENTS"
  #       k3s_help
  #     else
  #       echo "Failed to install $BASENAME"
  #     fi
  #     echo -e "\tFor support, go to https://github.com/mattiaperi/k3s-multipass-cluster/."
  #   fi
  #   cleanup
  #   exit $result
  # }
 
 # This is not currently used
 # helm_rbac_config {
  # cat << EOF | kubectl --kubeconfig="${KUBECONFIG}" apply -f -
  # apiVersion: v1
  # kind: ServiceAccount
  # metadata:
  #   name: tiller
  #   namespace: kube-system
  # ---
  # apiVersion: rbac.authorization.k8s.io/v1
  # kind: ClusterRoleBinding
  # metadata:
  #   name: tiller
  # roleRef:
  #   apiGroup: rbac.authorization.k8s.io
  #   kind: ClusterRole
  #   name: cluster-admin
  # subjects:
  #   - kind: ServiceAccount
  #     name: tiller
  #     namespace: kube-system
  # EOF
  # }
 
 