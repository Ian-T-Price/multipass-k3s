#!/usr/bin/env bash

kill "$(pgrep -fi "k3s.yaml proxy")" 2>/dev/null

kubectl --kubeconfig=/home/itp/.kube/k3s.yaml proxy &

