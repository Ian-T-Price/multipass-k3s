#!/usr/bin/env bash

# Get all the pods of a K8s cluster that are marked as 'Evicted' and delete them.

get-evicted-pods() {
  # produces the list in a format of: 'NameSpace' 'Pod Name'
  # k3s kubectl get po -A -o json | jq -r '.items[] | {Name: .metadata.name, Namespace: .metadata.namespace, Status: .status.phase} | select( any(. =="Running")) | [.Namespace, .Name] | @tsv'
  k3s kubectl get po -A -o json | jq -r '.items[] | {Name: .metadata.name, Namespace: .metadata.namespace, Status: .status.reason} | select( any(. =="Evicted")) | [.Namespace, .Name] | @tsv'
}

# See https://catonmat.net/bash-one-liners-explained-part-three for a very good treatise on File Descriptors
while read -r namespace name <&3; do
  {
    # k3s kubectl get pod -n "$namespace" "$name" -o wide
    k3s kubectl delete pod -n "$namespace" "$name"
  } 3>&-
done 3< <(get-evicted-pods)
