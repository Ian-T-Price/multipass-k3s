#!/usr/bin/env bash

# set -v
# set -x

# [[ $1 == "" ]] && SEARCHTERM="k3s" || SEARCHTERM="$1"
SEARCHTERM="$1"  # Update all images unless flter is supplied

for i in $(multipass list | grep -E "$SEARCHTERM" | grep -Evi "name" | awk -F' ' '{printf "%s ", $1}');
do
  echo -e "\n=== Updating $i ==="
  multipass exec "$i" -- sudo apt update
  multipass exec "$i" -- sudo apt upgrade -y
  multipass exec "$i" -- sudo apt autoremove -y
done

echo
multipass purge
multipass list
echo

# FILE=$(multipass exec $i -- sudo stat /var/run/reboot-required > /dev/null 2>&1)
if multipass exec "$i" -- sudo stat /var/run/reboot-required > /dev/null 2>&1
# if [ $? -eq 0 ]
then
  echo -e "\n !!! Re-boot required: rebooting now !!!"
  multipass exec "$i" -- sudo systemctl reboot
else
  echo -e "\nNo re-boot required"
fi

echo -e "\nUpdate local system"
sudo apt update
sudo apt upgrade -y 
sudo apt autoremove -y
