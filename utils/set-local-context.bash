#!/usr/bin/env bash

### Copy the k3s config (context) file to the local host 
### and then edit the IP address of the server URL to that of the Leading node

PROGRAM="$0"

if echo "$PROGRAM" | grep -q utils ; then
    # shellcheck source=./functions/helpers.bash
    source ./functions/helpers.bash
    # shellcheck source=./functions/config_kubectl.bash
    source ./functions/config_kubectl.bash
  else
    # shellcheck source=../functions/helpers.bash
    # shellcheck disable=SC1091
    source ../functions/helpers.bash
    # shellcheck source=../functions/config_kubectl.bash
    # shellcheck disable=SC1091
    source ../functions/config_kubectl.bash
fi

## if [[ $# -eq 0 ]]; then k3s_context_help; exit 0; fi
### Args ###
if [[ $# -eq 0 ]];
  then
  k3s_help; exit 0
  else
  set -u
  while [[ $# -gt 0 ]]; do
    case $1 in
      -h | '--help' ) k3s_help; exit 0 ;;
      -v | '--verbose' ) VERBOSE_LEVEL=1 ;;
      -vv) VERBOSE_LEVEL=2 ;;
      -vvv) VERBOSE_LEVEL=3 ;;
      
      *) exit 1
      ;;
    esac
    shift
  done
  set +u
fi

## LEADING_NODE
# Assumes that the master node name contains 'master'
# Add an option to change that 

LEADING_NODE="$(multipass list --format json | jq -rc '.list[] | {IP4Addr: .ipv4[0], Name: .name} | select( any(. | contains("master"))) | .Name')"
K3S_NODEIP_MASTER=$(multipass info "${LEADING_NODE}" | grep "IPv4" | awk -F' ' '{print $2}') && info "K3S_NODEIP_MASTER=${K3S_NODEIP_MASTER}"
K3S_URL="https://${K3S_NODEIP_MASTER}:6443" && info "K3S_URL=${K3S_URL}"

if [[ "${LEADING_NODE}" ]]; then
    export LEADING_NODE
    info "The master node name is: $LEADING_NODE"
  else
    fatal "No master node identified; multipass list:"
    mutlipass list
fi

## KUBECTL_INSTALLED

  if command -v kubectl > /dev/null 2>&1; then
    # echo -e "Kubectl ${fmt_cyan:?}Local ${fmt_end:?}version:"; k3s kubectl version # -o json
    KUBECTL_INSTALLED=true
    export KUBECTL_INSTALLED
  else
    warn "Local kubectl does not appear to be installed"
  fi
    
## KUBECONFIG
KUBECONFIG="$HOME/.kube/k3s.yaml"
export KUBECONFIG

## K3S_URL


kubectl_configuration