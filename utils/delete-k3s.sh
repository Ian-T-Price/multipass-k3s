#!/usr/bin/env bash

# set -v
# set -x

kill "$(pgrep -fi "k3s.yaml proxy" | grep -Ei kubectl | awk '{print $2}')" 2>/dev/null

[[ $1 == "" ]] && SEARCHTERM="k3s" || SEARCHTERM="$1"

for i in $(multipass list | grep -E "$SEARCHTERM" | awk -F' ' '{printf "%s ", $1}');
do
  echo -e "\n=== Deleting $i ==="
  multipass delete "$i"
done

echo
multipass purge
multipass list
echo