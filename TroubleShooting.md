# Trouble Shooting a K3s cluster set-up

### [Issue] The connection to the server x.x.x.x:6443 was refused - did you specify the right host or port?

### !The kubectl should be executed on the Master Node

[Check] env | grep -i kube
- KUBECONFIG=/home/itp/.kube/k3s.yaml
- KUBECONFIG=/etc/rancher/k3s/k3s.yaml

[Check] systemctl status containerd

[Check] k3s kubectl get componentstatuses
```
NAME                 STATUS    MESSAGE              ERROR
controller-manager   Healthy   ok                   
scheduler            Healthy   ok                   
etcd-0               Healthy   {"health": "true"}
```

`k3s kubectl -n cattle-system describe certificate`

https://www.thegeekdiary.com/troubleshooting-kubectl-error-the-connection-to-the-server-x-x-x-x6443-was-refused-did-you-specify-the-right-host-or-port/

