#!/usr/bin/env bash

### Multipass CRUD
### Function to help the management of multipass VMs 

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

# --- multipass setup --- 
function multipass_create_nodelist  {
  # $1 base part of name
  # $2 index of name
  K3S_NAME=$1$2

  CHK_INSTALL=$(multipass info "$K3S_NAME" 2>/dev/null)
  SUB='Name:'
  if [[ "$CHK_INSTALL" == *"$SUB"* ]]; then
    warn "=== $K3S_NAME: VM already exists"
    if [[ $K3S_NAME == $NAME_M"1" ]]; then
      info "Leading Node: Setting cluster data"
      LEADING_NODE=${K3S_NAME} && info "LEADING_NODE=${LEADING_NODE}"
      K3S_NODEIP_MASTER=$(multipass info "${LEADING_NODE}" | grep "IPv4" | awk -F' ' '{print $2}') && info "K3S_NODEIP_MASTER=${K3S_NODEIP_MASTER}"
      K3S_URL="https://${K3S_NODEIP_MASTER}:6443" && info "K3S_URL=${K3S_URL}"
      K3S_TOKEN="$(multipass exec "${LEADING_NODE}" -- /bin/bash -c "sudo cat /var/lib/rancher/k3s/server/node-token" < /dev/null)" && info "K3S_TOKEN=${K3S_TOKEN}"
    fi
  else
    info "=== $K3S_NAME: Adding VM to creation list"
    if [[ ${K3S_NAME}  == *"master"* ]]; then
      NODELIST=$NODELIST"$K3S_NAME $K3S_CPUS_M $K3S_MEM_M $K3S_DISK_M $K3S_IMAGE_M\n"
    else
      NODELIST=$NODELIST"$K3S_NAME $K3S_CPUS_W $K3S_MEM_W $K3S_DISK_W $K3S_IMAGE_W\n"
    fi
  fi
}

function multipass_delete  {
  # $1 base part of name
  # $2 count required
  local COUNT; COUNT=$(multipass list | grep -c "^$1")  # https://github.com/koalaman/shellcheck/wiki/SC2155
  info "=== Base name = $1 : Existing VM count = $COUNT : Desired VM count = $2 "

  for ((i=COUNT; i>"$2"; i--))
  do
    warn "=== $1$i: Deleting VM"
    multipass delete "$1$i"
  done
}
