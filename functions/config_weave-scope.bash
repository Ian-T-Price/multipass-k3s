#!/usr/bin/env bash

### Weave-Scope CRUD

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

function weave-scope_install {
  [[ ${NO_DEPLOY_WEAVESCOPE} != "true" ]] || return 0
  
  info "[weavescope] Install"
  kubectl --kubeconfig="${KUBECONFIG}" apply -f "https://cloud.weave.works/k8s/scope.yaml?k8s-version=$(kubectl --kubeconfig="${KUBECONFIG}" version | base64 | tr -d '\n')&k8s-service-type=NodePort"
  if kubectl --kubeconfig="${KUBECONFIG}" rollout status deployment weave-scope-app --namespace weave -w
    then success "[weavescope] installation: OK"
  else
    fatal "[weavescope] installation: KO"
  fi
  info "[weavescope] URL: http://${K3S_NODEIP_MASTER}:$(kubectl --kubeconfig="${KUBECONFIG}" get services weave-scope-app --namespace weave -o jsonpath="{.spec.ports[0].nodePort}")"
}