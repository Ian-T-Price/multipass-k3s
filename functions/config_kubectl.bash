#!/usr/bin/env bash

### Copy the k3s config (context) file to the local host
### and then edit the IP address of the server URL to that of the Leading node

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

function kubectl_configuration {
  if [[ "${KUBECTL_INSTALLED}" ]]; then
    if [[ -w "${KUBECONFIG}" ]]; then
        multipass copy-files "${LEADING_NODE}":/etc/rancher/k3s/k3s.yaml "${KUBECONFIG}"
        # Managing both localhost and 127.0.0.1 since K3S 0.9.0: https://github.com/rancher/k3s/pull/750
        sed -i -e "s|    server: https://localhost:6443|    #EDITED BY SCRIPT\n    server: ${K3S_URL}|" "${KUBECONFIG}"
        sed -i -e "s|    server: https://127.0.0.1:6443|    #EDITED BY SCRIPT\n    server: ${K3S_URL}|" "${KUBECONFIG}"
        if kubectl --kubeconfig="${KUBECONFIG}" get nodes
          then
            success "kubectl configuration: OK"
            info "\nUse i.e.: \"kubectl --kubeconfig=""${KUBECONFIG}"" get nodes\" or \"export KUBECONFIG=""${KUBECONFIG}""\""
          else
            (fatal "kubectl configuration: KO")
          fi
      else
        warn "kubectl not configured because not \"${KUBECONFIG}\" not writable"
        warn "Use i.e.: \"multipass exec ${LEADING_NODE} kubectl cluster-info\""
    fi
  else
    info "kubectl not configured because not installed"
    info "Use i.e.: \"multipass exec ${LEADING_NODE} kubectl cluster-info\""
  fi
}
