#!/usr/bin/env bash

### Copy this file to a new file 
### And add your function(s) and config(s) to that file

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

function dashboard_install {
    [[ "${NO_DEPLOY_DASHBOARD}" != "true" ]] || return 0
    #ref: https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/#deploying-the-dashboard-ui
    #ref: https://rancher.com/docs/k3s/latest/en/installation/kube-dashboard/
    
    #Create the dashboard
    DASHBOARD_LOG="$DASHBOARD_LOG $( kubectl --kubeconfig="${KUBECONFIG}" apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml )"
    
    # Create the dashboard service account
    DASHBOARD_LOG="$DASHBOARD_LOG $( kubectl --kubeconfig="${KUBECONFIG}" create serviceaccount dashboard-admin-sa )"
    
    # Bind the dashboard-admin-service-account service account to the cluster-admin role
    DASHBOARD_LOG="$DASHBOARD_LOG $( kubectl --kubeconfig="${KUBECONFIG}" create clusterrolebinding dashboard-admin-sa --clusterrole=cluster-admin --serviceaccount=default:dashboard-admin-sa )"
    
    # Get the complete secret name starting from a partial name
    SECRET_NAME=$(kubectl --kubeconfig="${KUBECONFIG}" get secrets | awk '/^dashboard-admin-sa/ {printf $1;exit}') # && info "SECRET_NAME=${SECRET_NAME}"
    SECRET_TOKEN=$(kubectl --kubeconfig="${KUBECONFIG}" get secret "${SECRET_NAME}" -o jsonpath="{.data.token}" | base64 --decode) # && info "SECRET_TOKEN=${SECRET_TOKEN}"
    #SECRET_TOKEN=$(kubectl --kubeconfig="${KUBECONFIG}" get secret "${SECRET_NAME}" -o json | jq -r '.data.token') && info "SECRET_TOKEN=${SECRET_TOKEN}"
    #nohup kubectl --kubeconfig="${KUBECONFIG}" proxy >/dev/null 2>&1 &
    
    # Set a sane timeout default for the the dashboard token
    DASHBOARD_LOG=$( kubectl --kubeconfig="${KUBECONFIG}" patch deploy kubernetes-dashboard -n kubernetes-dashboard --patch "$(cat config/dashboard-timeout-patch.yaml)")
    success "Dashboard created $DASHBOARD_LOG"
    info "Run:    $ kubectl --kubeconfig=${HOME}/.kube/k3s.yaml proxy"
    info "Open:   http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/"
    info "Token:  ${SECRET_TOKEN}"
}