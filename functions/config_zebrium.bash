#!/usr/bin/env bash

### Copy this file to a new file 
### And add your function(s) and config(s) to that file
# Requires Zebrium auth token to be set in environment variable ZEBRIUM_TOKEN
# See https://docs.zebrium.com/
# See https://docs.zebrium.com/docs/setup/kubernetes/

# See https://gist.github.com/so0k/42313dbb3b547a0f51a547bb968696ba for some excellent jq tips and the jid command


# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

function zebrium_install {
  # Check the installation status of Zebrium and the existance od the required Zebrium variables

  # k3s kubectl get daemonset -A -o json | jq -r '.items[].metadata | select( any(. == "zlog-collector" )) | .namespace'
  
  # TODO Test what error we get if there is no cluster
  ZEBRIUM_DS=$(k3s kubectl get daemonset -A -o json | jq -r '.items[].metadata | select( any(. == "zlog-collector" )) | .namespace')
  
  if [[ "$ZEBRIUM_DS" ]];
    then
      info "Zebrium already installed in NameSpace $ZEBRIUM_DS"
    else
      if [[ $(k3s kubectl get ns -o json | jq -r --arg ZEBRIUM_NAMESPACE "$ZEBRIUM_NAMESPACE" '.items[].metadata | select( any(. ==$ZEBRIUM_NAMESPACE )) | .name') ]];
        then
          info "Zebrium namespace exists:\t$ZEBRIUM_NAMESPACE"
        else
          k3s kubectl create namespace "$ZEBRIUM_NAMESPACE"
          # TODO - correct status dependant on errorlevel of the above
          # info "Zebrium namespace created; status:\t$ZEBRIUM_NAMESPACE is $ZEBRIUM_STATUS_NS"
        fi
      
      # shellcheck disable=SC1091
      source "$ZEBRIUMCONFIG"
      ZEBRIUM_DEPLOYMENT="$ZEBRIUM_NAMESPACE-$ZEBRIUM_POSTFIX"
      sed -i -e "s|^ZEBRIUM_DEPLOYMENT=.*$|ZEBRIUM_DEPLOYMENT=\"$ZEBRIUM_DEPLOYMENT\"  # EDITED BY SCRIPT|" "${ZEBRIUMCONFIG}"
      
      # helm --kubeconfig $KUBECONFIG repo add zlog-collector https://raw.githubusercontent.com/zebrium/ze-kubernetes-collector/master/charts
      # helm --kubeconfig $KUBECONFIG --namespace zebrium uninstall zlog-collector

      helm install "zlog-collector-$ZEBRIUM_NAMESPACE"  zlog-collector \
      --namespace "$ZEBRIUM_NAMESPACE" \
      --repo https://raw.githubusercontent.com/zebrium/ze-kubernetes-collector/master/charts \
      --set zebrium.collectorUrl=https://zapi03.zebrium.com,zebrium.authToken="$ZEBRIUM_TOKEN",zebrium.deployment="$ZEBRIUM_DEPLOYMENT"
    fi
      
    ZEBRIUM_STATUS_NS=$(k3s kubectl get namespace "$ZEBRIUM_NAMESPACE" -o json | jq -r '.status.phase')
    ZEBRIUM_STATUS_PO=$(k3s kubectl get po --namespace "$ZEBRIUM_NAMESPACE" -o wide | grep -ic running)
    info "Zebrium namespace status:\t$ZEBRIUM_NAMESPACE is $ZEBRIUM_STATUS_NS"
    info "Zebrium deployment name:\t$ZEBRIUM_NAME"
    extra "Zebrium token:\t\t$ZEBRIUM_TOKEN"
    extra "Zebrium running pods:\t$ZEBRIUM_STATUS_PO  Nota Bene: you may need to wait awhile for these"
}
