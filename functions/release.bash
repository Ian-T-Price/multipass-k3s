#!/usr/bin/env bash

### Check the latest release of K3s
### And warn if we're not installing the latest

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

# --- release info ---
# shellcheck disable=SC2086 
function k3s_release_info {
  require_var INSTALL_K3S_VERSION "INSTALL_K3S_VERSION must be defined"
  LATEST_K3S_VERSION=$(curl -s https://api.github.com/repos/rancher/k3s/releases/latest | jq -r '.tag_name')
  info "Latest ${fmt_yellow:?}Stable${fmt_end:?} release: ${LATEST_K3S_VERSION})"
  if [[ "${LATEST_K3S_VERSION}" == "${INSTALL_K3S_VERSION}" ]]; then
    success "Installing latest release"
  else
    warn "A newer release is available: ${LATEST_K3S_VERSION}" && info "Installing: ${INSTALL_K3S_VERSION}"
  fi
  extra "$(curl -s https://api.github.com/repos/rancher/k3s/releases/tags/${INSTALL_K3S_VERSION} | jq -r '.body')"
}
