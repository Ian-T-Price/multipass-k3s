#!/usr/bin/env bash

### Copy this file to a new file 
### And add your function(s) and config(s) to that file

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

### UPDATE the comments above and remove these comments
### ENSURE you 'source' the new file in the main script
### ENSURE you add the '# shellcheck source={filename} line to the main script
### CHMOD +x the file to ensure the help function works


helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
helm repo add jetstack https://charts.jetstack.io
helm repo update

k3s kubectl create namespace cattle-system
k3s kubectl create namespace cert-manager

k3s kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.0/cert-manager.crds.yaml

# Install the cert-manager Helm chart
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version v0.15.0

k3s kubectl get pods --namespace cert-manager

helm install rancher rancher-latest/rancher \
  --namespace cattle-system \
  --set hostname=rancher.local

echo

k3s kubectl get po -A

echo

k3s kubectl -n cattle-system rollout status deploy/rancher