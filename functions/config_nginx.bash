#!/usr/bin/env bash

### Nginx CRUD

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

function nginx {
	cat <<- EOF | kubectl --kubeconfig="${KUBECONFIG}" apply -f -
	apiVersion: v1
	kind: Service
	metadata:
	  name: nginx-svc
	  labels:
	    app: nginx-svc
	spec:
	  selector:
	    app: nginx
	  ports:
	  - name: http
	    port: 80
	    nodePort: 32767
	  type: NodePort
	---
	apiVersion: apps/v1
	kind: Deployment
	metadata:
	  name: nginx-deploy
	  labels:
	    version: v1
	spec:
	  replicas: 1
	  selector:
	    matchLabels:
	      app: nginx
	  template:
	    metadata:
	      labels:
	        app: nginx
	        version: v1
	    spec:
	      containers:
	      - name: nginx-container
	        image: nginx
	        ports:
	        - containerPort: 80
	EOF
}