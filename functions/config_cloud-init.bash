#!/usr/bin/env bash

### Cloud-init CRUD 
### Set-up the initial configuration scripts for both masters & workers

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

function node_config {
  # Install Master Node(s)
  info "=== Adding MASTER NODES configuration ==="
  
  # Check the desired count is odd
  if (( MASTER_COUNT % 2 == 0 && MASTER_COUNT != 0 )); then (( MASTER_COUNT -= 1 )); fi
  
  multipass_delete "$NAME_M" "$MASTER_COUNT"
  
  for (( i=1; i<=MASTER_COUNT; i++ ))
  do
    multipass_create_nodelist "$NAME_M" "$i" # "$K3S_IMAGE_M"
  done
  
  # Install Worker Node(s)
  info "=== Adding WORKER NODES configuration ==="
  
  multipass_delete "$NAME_W" "$WORKER_COUNT"
  
  for (( i=1; i<=WORKER_COUNT; i++ ))
  do 
    multipass_create_nodelist "$NAME_W" "$i" # "$K3S_IMAGE_W"
  done
  info "Nodelist:\n$NODELIST"
}

function k3s_cloud_init_base  {
  # Use this section to add configuration that you want to appear in both master & worker nodes
  # shellcheck disable=2154
  cat <<- EOF | tee cloud-init/k3s-master.yaml cloud-init/k3s-worker.yaml > /dev/null 
	#coud-config
	
	package_update: true
	package_upgrade: true
	package_reboot_if_required: true
	
	snap:
	  commands:
	    00: ['install', 'jq', '--beta']
	
	write_files:
	  # https://bugs.launchpad.net/cloud-init/+bug/1486113?comments=all
	  # Due to bug above we write to the skeleton files and not to /home/ubuntu
	  - encoding: ""
	    path: /etc/skel/.bash_aliases
	    permissions: '0644'
	    append: true
	    content: |
	      #!/usr/bin/env bash
	
	      alias egrep='egrep --color=auto'
	      alias es='clear;env|sort -r' 
	      alias ll='ls -lsa'
	      alias treed='tree -pughCaD'
	      alias kk='k3s kubectl'
	      alias kkn='k3s kubectl get nodes'
	      alias kkp='k3s kubectl get pods'
	      alias kkpa='k3s kubectl get pods -A'
	      alias psk='ps -ef | egrep k3s'
	      alias ssk='systemctl status k3s'
	      alias srk='systemctl restart k3s'
	      alias jxe='journalctl -xe'
	      alias juk='journalctl -u k3s.service'
	  
	  - encoding: ""
	    path: /etc/skel/.bashrc
	    permissions: '0644'
	    append: true
	    content: |
	      if [ -f "$HOME/.bash-git-prompt/gitprompt.sh" ]; then
	        GIT_PROMPT_ONLY_IN_REPO=1
	        source $HOME/.bash-git-prompt/gitprompt.sh
	      fi
	      
	      # Set a nice, usable prompt
	      export PS1="\e[95m${debian_chroot:+($debian_chroot)}\[\033[0;33m\]\w\[\033[0;0m\] \u\[\033[0;35m\]@\h\n\[\033[0;37m\]$(date +%H:%M)\[\033[0;0m\] $ "
	
	packages:
	  # https://askubuntu.com/questions/1178918/tree-command-not-working-in-some-directories
	  - tree
	
	EOF
}

function k3s_cloud_init_master {
  # Use this section to add configuration that you ONLY want to appear in  master nodes
  # This should and did run via Bash process substitution but that stopped working
  # so now we have this cludge working via a file... 
	#- '\curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" INSTALL_K3S_VERSION=${INSTALL_K3S_VERSION} K3S_URL=${K3S_URL} sh -s -'
  # tee --append
  cat <<- EOF | tee --append cloud-init/k3s-master.yaml > /dev/null
	runcmd:
	 - export INSTALL_K3S_EXEC="server --cluster-init"
	 - source /var/lib/cloud/instance/scripts/runcmd
	 - $K3S_EXEC
	EOF
	  # Worker mode
	  #- '\curl -sfL https://get.k3s.io | K3S_TOKEN=${K3S_TOKEN} INSTALL_K3S_VERSION=${INSTALL_K3S_VERSION} K3S_URL=${K3S_URL} sh -s -'
	  # Master mode - FIRST
	  #- '\curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" INSTALL_K3S_VERSION=${INSTALL_K3S_VERSION} sh -s - --node-taint node-role.kubernetes.io/master=effect:NoSchedule'
	  # Master mode - SUBSEQUENT
	  #- '\curl -sfL https://get.k3s.io | K3S_TOKEN=${K3S_TOKEN} INSTALL_K3S_VERSION=${INSTALL_K3S_VERSION} K3S_URL=${$K3S_NODEIP_MASTER} K3S_KUBECONFIG_MODE="644" INSTALL_K3S_EXEC=${K3S_EXEC} sh -s - --node-taint node-role.kubernetes.io/master=effect:NoSchedule'
}

function k3s_cloud_init_worker {
  # Use this section to add configuration that you ONLY want to appear in  worker nodes
  # This should and did run via Bash process substitution but that stopped working
  # so now we have this cludge working via a file... 
  cat <<- EOF | tee --append cloud-init/k3s-worker.yaml > /dev/null
		runcmd:
	  #- '\curl -sfL https://get.k3s.io | K3S_TOKEN=${K3S_TOKEN} INSTALL_K3S_VERSION=${INSTALL_K3S_VERSION} K3S_URL=${K3S_URL} sh -s -'
	  - '\curl -sfL https://get.k3s.io | K3S_TOKEN=${K3S_TOKEN} INSTALL_K3S_VERSION=${INSTALL_K3S_VERSION} K3S_URL=${K3S_URL} sh -s -'
	EOF
}
