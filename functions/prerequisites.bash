#!/usr/bin/env bash

### Check that the basic requirements for creating a k3s cluster are in place
### And take actions to fix any missing requirements

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

# --- check root user ---
function check_root {
  if [[ $USER == root || $HOME == /root ]] ; then
    fatal "Please don't run as root. Sudo will be used internally by this script as required."
    exit 1
  fi
}

# -- Install App to the right OS ---
function installer {
  # Check the OS flavour and run the appropriate commands
  # TODO Get correct install types for other OSs
  case "$OSTYPE" in
    linux*) info "The OS is a flavour of ${fmt_green}${fmt_bold}Linux${fmt_end}; ${fmt_cyan}${fmt_bold}Installing $1 ${fmt_end}"
      if [[ $(sudo snap install "$1" --classic 1> /dev/null) == 0 ]]; 
        then
          fatal "Snap: Application ${fmt_cyan}${fmt_bold} $1 ${fmt_end} is required but failed to install"
          exit 1
        else
          info "Snap: Application ${fmt_cyan}${fmt_bold} $1 ${fmt_end} installed successful"
          declare -g "$APP_U""_INSTALLED"=true
          declare -g TMP=$APP_U"_INSTALLED"
          success "App $TMP = ${!TMP}  $APP_U version: $(snap list "$APP_L" | grep -e "$APP_L")"
        fi
        ;;
    freebsd*)  info "The OS is a flavour of BSD Unix" ;;
    bsd*)      info "The OS is a flavour of BSD Unix" ;;
    solaris*)  info "The OS is a flavour of SunOS" ;;
    darwin*)   info "The OS is a flavour of Mac OSX" 
               #  "$(brew cask install multipass 2>/dev/null)"
               ;;
    cygwin*)   info "The OS is a flavour of POSIX compatibility layer and Linux environment emulation for Windows" ;;
    msys*)     info "The OS is a flavour of Lightweight shell and GNU utilities compiled for Windows (part of MinGW)" ;;
    win32*)    info "The OS is a flavour of Windows - can this happen?" ;;
    *)         info "The OS Not recognised" ; exit 1 ;;
  esac
}

# -- Check installed Apps ---
function installed {
  APP_L="$(lowercase "$1")"
  APP_U="$(uppercase "$1" )"
  if [[ $(type -p "${APP_L?}") ]];
    then
      declare -g "$APP_U""_INSTALLED"=true
      declare -g TMP=$APP_U"_INSTALLED"
      success "App $TMP = ${!TMP}  $APP_U version: $(snap list "$APP_L" | grep -e "$APP_L")"
    else
      declare -g "$APP_U""_INSTALLED="false
      declare -g TMP=$APP_U"_INSTALLED"
      warn "App $APP_U installed: ${!TMP}"
      installer "$APP_L"
  fi
  success "Prerequisites: OK"
}
