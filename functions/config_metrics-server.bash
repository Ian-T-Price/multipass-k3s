#!/usr/bin/env bash

### Metrics Server CRUD
### metrics-server_install 
### OBSOLETE DUE TO: https://github.com/rancher/k3s/issues/990

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

function metrics-server_rbac_config {
	cat <<- EOF | kubectl --kubeconfig="${KUBECONFIG}" apply -f -
	apiVersion: v1
	kind: ServiceAccount
	metadata:
	  name: metrics-server
	  namespace: kube-system
	---
	apiVersion: rbac.authorization.k8s.io/v1
	kind: ClusterRoleBinding
	metadata:
	  name: metrics-server
	roleRef:
	  apiGroup: rbac.authorization.k8s.io
	  kind: ClusterRole
	  name: cluster-admin
	subjects:
	  - kind: ServiceAccount
	    name: metrics-server
	    namespace: kube-system
	EOF
}

function metrics-server_install {
  info "[metrics-server] Configuring ServiceAccount and ClusterRoleBinding"
  metrics-server_rbac_config
  info "[metrics-server] Install"
  TMP_DIR=$(mktemp -d /tmp/k3s-XXXXXX) && cd "$TMP_DIR" && echo "$TMP_DIR"
  git clone https://github.com/kubernetes-incubator/metrics-server.git
  kubectl --kubeconfig="${KUBECONFIG}" apply -f metrics-server/deploy/1.8+/
  if kubectl --kubeconfig="${KUBECONFIG}" rollout status deployment metrics-server --namespace kube-system -w
    then
      success "[metrics-server] installation: OK"
    else
      fatal "[metrics-server] installation: KO"
  fi
  rm -rf "$TMP_DIR"
  info "[metrics-server] Awaiting for the server to be ready (and avoid \"the server is currently unable to handle the request\" error)"
  until kubectl --kubeconfig="${KUBECONFIG}" top nodes >/dev/null 2>&1; do echo -n .; sleep 5; done; echo; info "[metrics-server] installation: server up & running"
}
