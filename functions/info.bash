#!/usr/bin/env bash

### Provide information about the k3s cluster
### Use -v, -vv or -vvv to gain additional information

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

function info_block {
  # Multipass details
  echo -e "\n${fmt_green:?}${fmt_bold:?}Multipass configuration:${fmt_end:?}"
  if command -v multipass > /dev/null 2>&1; then
    multipass version
    multipass list
  else
    warn "Multipass does not appear to be installed"
  fi

  # K3s & Kubernetes details
  echo -e "\n${fmt_green:?}${fmt_bold:?}K3s & K8s configuration:${fmt_end:?}"
  if command -v k3s > /dev/null 2>&1; then
    k3s --version
  else
    warn "k3s does not appear to be installed"
  fi
  
  if command -v kubectl > /dev/null 2>&1; then
    echo -e "${fmt_cyan:?}[NOTE] ${fmt_end:?}K3s links the 'kubectl' command to the 'k3s' command if 'kubectl' is not installed"
    echo -e "${fmt_cyan:?}[NOTE] ${fmt_end:?}This can be confirmed by checking the 'gitVersion' of 'kubectl' below. They are linked if the version includes 'k3s'"
    echo -e "Kubectl version:"; k3s kubectl version # -o json
  else
    warn "kubectl does not appear to be installed"
  fi
    
  # Dashboard instructions
  SECRET_NAME=$(kubectl --kubeconfig="${KUBECONFIG}" get secrets | awk '/^dashboard-admin-sa/ {printf $1;exit}')
  SECRET_TOKEN=$(kubectl --kubeconfig="${KUBECONFIG}" get secret "${SECRET_NAME}" -o jsonpath="{.data.token}" | base64 --decode)
  echo -e "\n${fmt_green:?}${fmt_bold:?}To run the Kubernetes Dashboard:${fmt_end:?}"
  echo -e "Run:    $ kubectl --kubeconfig=${HOME}/.kube/k3s.yaml proxy"
  echo -e "Open:   http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/"
  echo -e "Token:  ${SECRET_TOKEN}"

  # K8s Cluster details
  echo -e "\n${fmt_green:?}${fmt_bold:?}K8s cluster details:${fmt_end:?}"
  echo -e; k3s kubectl get namespace | body sort
  echo -e; k3s kubectl get nodes | body sort
  echo -e; k3s kubectl get pods -A | body sort
}