#!/usr/bin/env bash

### Prometheus CRUD

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

function prometheus-operator_install {
  [[ ${NO_DEPLOY_PROMETHEUS} != "true" ]] || return 0
  
  info "[prometheus] Create \"monitoring\" namespace"
  kubectl --kubeconfig="${KUBECONFIG}" create namespace monitoring
  info "[prometheus] Add helm stable repository"
  helm repo add stable https://kubernetes-charts.storage.googleapis.com
  info "[prometheus] Install CRDs https://github.com/helm/charts/tree/master/stable/prometheus-operator#helm-fails-to-create-crds"
  kubectl --kubeconfig="${KUBECONFIG}" apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/monitoring.coreos.com_alertmanagers.yaml
  kubectl --kubeconfig="${KUBECONFIG}" apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/monitoring.coreos.com_prometheuses.yaml
  kubectl --kubeconfig="${KUBECONFIG}" apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/monitoring.coreos.com_prometheusrules.yaml
  kubectl --kubeconfig="${KUBECONFIG}" apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/monitoring.coreos.com_servicemonitors.yaml
  kubectl --kubeconfig="${KUBECONFIG}" apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/monitoring.coreos.com_podmonitors.yaml
  info "[prometheus] Install"
  if helm --kubeconfig="${KUBECONFIG}" install prometheus-operator stable/prometheus-operator --namespace monitoring --set prometheusOperator.createCustomResource=false --set grafana.service.type=NodePort --set grafana.service.nodePort=30808 --set prometheus.service.type=NodePort --set prometheus.service.nodePort=30909 --set kubelet.serviceMonitor.https=true --wait --timeout 300s
    then
      success "[prometheus] installation: OK"
    else
      fatal "[prometheus] installation: KO"
  fi
  info "[prometheus] URL Grafana: http://${K3S_NODEIP_MASTER}:$(kubectl --kubeconfig="${KUBECONFIG}" get services prometheus-operator-grafana --namespace monitoring -o jsonpath="{.spec.ports[0].nodePort}"), user: \"$(kubectl --kubeconfig="${KUBECONFIG}" get secrets prometheus-operator-grafana --namespace monitoring -o jsonpath='{.data.admin-user}' | base64 --decode)\", pwd: \"$(kubectl --kubeconfig="${KUBECONFIG}" get secrets prometheus-operator-grafana --namespace monitoring -o jsonpath='{.data.admin-password}' | base64 --decode)\""
  info "[prometheus] URL Prometheus: http://${K3S_NODEIP_MASTER}:$(kubectl --kubeconfig="${KUBECONFIG}" get services prometheus-operator-prometheus --namespace monitoring -o jsonpath="{.spec.ports[0].nodePort}")"
}