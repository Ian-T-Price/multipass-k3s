#!/usr/bin/env bash

### Istio CRUD
### Istio main and Istio components configuration

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

# shellcheck disable=2046
function istio_install {
  [[ ${NO_DEPLOY_ISTIO} != "true" ]] || return 0
  
  if ${HELM_INSTALLED}; then
    info "[istio.io] Installation ref: https://istio.io/docs/setup/kubernetes/install/helm/"
    info "[istio.io] Create \"istio-system\" namespace"
    kubectl --kubeconfig="${KUBECONFIG}" create namespace istio-system
    info "[istio.io] Adding helm repo"
    helm repo add istio.io https://storage.googleapis.com/istio-release/releases/1.2.0/charts/
    info "[istio.io] Installing required CRDs"
    helm --kubeconfig="${KUBECONFIG}" install istio-init --namespace istio-system istio.io/istio-init --wait --timeout 300s
    while [ $(kubectl --kubeconfig="${KUBECONFIG}" get crds | grep -c 'istio.io\|certmanager.k8s.io' | xargs 2>/dev/null) -ne 23 ]; do echo -n .; sleep 5; done; echo; info "[istio.io] Required CRDs have been committed"
    info "[istio.io] Install chart"
    # --set kiali.enabled=true
    # HELM BUG: https://github.com/helm/helm/issues/6894 Helm v2.16.0 --> Error: no kind "Job" is registered for version "batch/v1" in scheme "k8s.io/kubernetes/pkg/api/legacyscheme/scheme.go:30"
    # WAITING TO BE FIXED
    if helm --kubeconfig="${KUBECONFIG}" install --name istio --namespace istio-system --set grafana.enabled=true istio.io/istio --wait --timeout 600
        then
          success "[istio.io] installation: OK"
        else
          info "[istio.io] installation: KO *********** CHECK IS NEEDED ***********"
    fi
    info "[istio.io] Enabling the creation of Envoy proxies for automatic sidecar injection"
    kubectl --kubeconfig="${KUBECONFIG}" label namespace default istio-injection=enabled
    kubectl --kubeconfig="${KUBECONFIG}" get namespace -L istio-injection
    info "[istio.io] Creating Istio Objects"
    istio_grafana_gateway
    istio_grafana_virtualservice
    while [ $(curl -o /dev/null -w "%{http_code}\n" -sSLIk "http://${K3S_NODEIP_WORKER}:15031" 2>/dev/null) -ne 200 ]; do echo -n .; sleep 5; done; echo; info "[istio.io] Istio graphana: http://${K3S_NODEIP_WORKER}:15031"
    istio_nginx_gateway
    istio_nginx_virtualservice
    nginx
    while [ $(curl -o /dev/null -w "%{http_code}\n" -sSLIk "http://${K3S_NODEIP_WORKER}" 2>/dev/null) -ne 200 ]; do echo -n .; sleep 5; done; echo; info "[istio.io] nginx: http://${K3S_NODEIP_WORKER}"
  else
      info "[istio.io] istio not configured because helm is not installed"
  fi
}

function istio_grafana_gateway {
	cat <<- EOF | kubectl --kubeconfig="${KUBECONFIG}" apply -f -
	apiVersion: networking.istio.io/v1alpha3
	kind: Gateway
	metadata:
	  name: grafana-gateway
	  namespace: istio-system
	spec:
	  selector:
	    istio: ingressgateway
	  servers:
	  - port:
	      number: 15031
	      name: http-grafana
	      protocol: HTTP
	    hosts:
	    - "*"
	EOF
}

function istio_grafana_virtualservice {
	cat <<- EOF | kubectl --kubeconfig="${KUBECONFIG}" apply -f -
	apiVersion: networking.istio.io/v1alpha3
	kind: VirtualService
	metadata:
	  name: grafana-vs
	  namespace: istio-system
	spec:
	  hosts:
	  - "*"
	  gateways:
	  - grafana-gateway
	  http:
	  - match:
	  - port: 15031
	  route:
	  - destination:
        host: grafana
        port:
          number: 3000
	EOF
}

function istio_nginx_gateway {
	cat <<- EOF | kubectl --kubeconfig="${KUBECONFIG}" apply -f -
	apiVersion: networking.istio.io/v1alpha3
	kind: Gateway
	metadata:
	  name: nginx-gateway
	spec:
	  selector:
	    istio: ingressgateway 
	  servers:
	  - port:
	      number: 80
	      name: http
	      protocol: HTTP
	    hosts:
	    - "*"
	EOF
}

function istio_nginx_virtualservice {
	cat <<- EOF | kubectl --kubeconfig="${KUBECONFIG}" apply -f -
	apiVersion: networking.istio.io/v1alpha3
	kind: VirtualService
	metadata:
	  name: nginx-vs
	spec:
	  hosts:
	  - "*"
	  gateways:
	  - nginx-gateway
	  http:
	  - route:
	    - destination:
	      host: nginx-svc
	EOF
}
