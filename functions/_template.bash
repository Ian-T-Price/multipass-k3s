#!/usr/bin/env bash

### Copy this file to a new file 
### And add your function(s) and config(s) to that file

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

### UPDATE the comments above and remove these comments
### ENSURE you 'source' the new file in the main script
### ENSURE you add the '# shellcheck source={filename} line to the main script
### CHMOD +x the file to ensure the help function works