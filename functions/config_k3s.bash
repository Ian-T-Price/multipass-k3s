#!/usr/bin/env bash

### K3s CRUD functions
### The script that actually creates the masters and workers
# https://github.com/rancher/k3s/archive/v1.17.5+k3s1.tar.gz

# shellcheck source=./functions/helpers.bash
source ./functions/helpers.bash

if [[ $# -eq 0 ]]; then k3s_help; exit 0; fi

# shellcheck disable=2016
function k3s_master_node {
  K3S_NAME=$1
  K3S_CPUS=$2
  K3S_MEM=$3
  K3S_DISK=$4
  K3S_IMAGE=$5

  if [[ $K3S_NAME == $NAME_M"1" ]]; then
    K3S_EXEC="curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE=644 INSTALL_K3S_VERSION=${INSTALL_K3S_VERSION} sh -s -"
    # TODO This should be a seaparate function to taint and untanint master nodes to make the script immutable
    if [[ $WORKER_COUNT != 0 ]]; then K3S_EXEC="$K3S_EXEC --node-taint node-role.kubernetes.io/master=effect:NoSchedule"; fi
  else
    info "Leading Node   : $LEADING_NODE"
    info "Leading Node IP: $K3S_NODEIP_MASTER"
    K3S_EXEC="curl -sfL https://get.k3s.io | K3S_TOKEN=${K3S_TOKEN} K3S_KUBECONFIG_MODE=644 INSTALL_K3S_VERSION=${INSTALL_K3S_VERSION} INSTALL_K3S_EXEC='server --server $K3S_URL' sh -s - --node-taint node-role.kubernetes.io/master=effect:NoSchedule"
  fi
  info "Creating $K3S_NAME with runcmd:\n\t$K3S_EXEC"
  k3s_cloud_init_master
  
  #multipass launch --name ${K3S_NAME} --cpus ${K3S_CPUS} --mem ${K3S_MEM} --disk ${K3S_DISK} --cloud-init <(k3s_cloud_init_master) $K3S_IMAGE
  # The above bash substition failed on 2020-04-23 (Update of Linux?) so resorting to writing out to .yaml files
  if multipass launch --name "${K3S_NAME}" --cpus "${K3S_CPUS}" --mem "${K3S_MEM}" --disk "${K3S_DISK}" --cloud-init cloud-init/k3s-master.yaml "$K3S_IMAGE"
    then
      success "Node ${K3S_NAME} k3s creation: OK"
    else 
      fatal "Node ${K3S_NAME} creation: KO"
  fi
  info "Node ${K3S_NAME} k3s: Waiting to be ready"
  if multipass exec "${K3S_NAME}" -- /bin/bash -c 'while [[ $(k3s kubectl get nodes $(hostname) --no-headers 2>/dev/null | grep -c -w "Ready") -ne 1 ]]; do echo -n .; sleep 5; done; echo' < /dev/null
    then
      success "Node ${K3S_NAME} k3s: Ready"
    else
      fatal "Node ${K3S_NAME} k3s: KO"
  fi
}

function k3s_node {
  K3S_NAME=$1
  K3S_CPUS=$2
  K3S_MEM=$3
  K3S_DISK=$4
  K3S_IMAGE=$5
  
  #k3s_cloud_init_worker
  cat <(k3s_cloud_init_worker)

  # multipass launch --name ${K3S_NAME} --cpus ${K3S_CPUS} --mem ${K3S_MEM} --disk ${K3S_DISK} --cloud-init <(k3s_cloud_init_worker) $5
  # The above bash substition failed on 2020-04-23 (Update of Linux?) so resorting to writing out to .yaml files
  if multipass launch --name "${K3S_NAME}" --cpus "${K3S_CPUS}" --mem "${K3S_MEM}" --disk "${K3S_DISK}" --cloud-init cloud-init/k3s-worker.yaml "$K3S_IMAGE"
    then
      success "Node ${K3S_NAME} k3s creation: OK"
    else
      fatal "Node ${K3S_NAME} creation: KO"
  fi
  info "Node ${K3S_NAME} k3s: Waiting to be ready"
  # kubectl is configured only on master node
  # we need double apex to pass variables to the string
  # we need backslash on kubectl otherwise executed on host machine
  if multipass exec "${LEADING_NODE}" -- /bin/bash -c "while [[ \$(kubectl get nodes ${K3S_NAME} --no-headers 2>/dev/null | grep -c -w \"Ready\") -ne 1 ]]; do echo -n .; sleep 5; done; echo" < /dev/null
    then
      success "Node ${K3S_NAME} k3s: Ready"
    else
      fatal "Node ${K3S_NAME} k3s: KO"
    fi
}

function k3s_setup {
  while read -r -a LINE; do
    info "Setup node: ${LINE[*]}"
    if [[ ${LINE[0]} == *"$NAME_M"* ]]; then
      k3s_master_node "${LINE[@]}"
      if [[ -z "$LEADING_NODE" ]]; then
        LEADING_NODE=${LINE[0]} && info "LEADING_NODE=${LEADING_NODE}"
        K3S_NODEIP_MASTER=$(multipass info "${LEADING_NODE}" | grep "IPv4" | awk -F' ' '{print $2}') && info "K3S_NODEIP_MASTER=${K3S_NODEIP_MASTER}"
        K3S_URL="https://${K3S_NODEIP_MASTER}:6443" && info "K3S_URL=${K3S_URL}"
        K3S_TOKEN=$(multipass exec "${LEADING_NODE}" -- /bin/bash -c "sudo cat /var/lib/rancher/k3s/server/node-token" < /dev/null) && info "K3S_TOKEN=${K3S_TOKEN}"
      fi
    fi
    if [[ ${LINE[0]} == *"$NAME_W"* ]]; then
      k3s_node "${LINE[@]}"
      K3S_NODEIP_WORKER=$(multipass info "${LINE[0]}" | grep "IPv4" | awk -F' ' '{print $2}') && info "K3S_NODEIP_WORKER=${K3S_NODEIP_WORKER}"
    fi
  done < <(echo -e "$NODELIST")
}

function k3s_labels {
 # OBSOLETE DUE TO: https://github.com/rancher/k3s/issues/379
 # NOT-SO-OBSOLETE ANYMORE, DUE TO: https://github.com/kubernetes/kubernetes/issues/55232
 ### nodes labels and taints
 while read -r -a LINE; do
   if [[ ${LINE[0]} == *"master"* ]]; then
     info "Node ${LINE[0]} labels and taints"
     # multipass exec ${LINE[0]} -- /bin/bash -c 'kubectl label node $(hostname) node-role.kubernetes.io/master=""' < /dev/null
     # multipass exec ${LINE[0]} -- /bin/bash -c 'kubectl taint node $(hostname) node-role.kubernetes.io/master=effect:NoSchedule' < /dev/null
   fi
   if [[ ${LINE[0]} == *"worker"* ]]; then
     info "Node ${LINE[0]} labels"
     multipass exec "${LEADING_NODE}" -- /bin/bash -c "kubectl label node ${LINE[0]} node-role.kubernetes.io/node=\"\"" < /dev/null
   fi
 done < <(node_config) 
}