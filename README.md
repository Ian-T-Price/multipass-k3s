# multipass-k3s
## Bash version

### <a name="TopOfForm"></a>Create a Rancher K3s multi-master cluster using  multipass VMs

  - Uses [Multipass](https://multipass.run/docs/working-with-instances)
  - Immutable. Almost ;-)

I started to write a bash script to create K3s clusters with multipass.  
Unsurprisingly, when searching for relevant ideas,  I came across [Mattia Peri](https://levelup.gitconnected.com/@mattiaperi)'s
git repo on 2020-05-01 which had done much of the heavy lifting in the same style that I was
creating.

However, Mattia's approach was Mac centric (Why?  Easy to generalise the code),
not Immutable (running the script again actually deleted the existing set-up -
dangerous!) and it did not cater for multi-master set-ups.

Thus I forked his code and started on the repo.

This first version qualifies as a Minimum Viable Product (MVP) which will create:

  - A K3s cluster with one master and three workers by default

## To Do

These have now been moved to the Issues queue on the repo

## Useful links

[TView - pontential front end to serve all the options](https://pkg.go.dev/mod/github.com/rivo/tview)

[Rancher K3s Installation Options](https://rancher.com/docs/k3s/latest/en/installation/install-options/)
[Rancher Management Server Installation](https://rancher.com/docs/rancher/v2.x/en/installation/k8s-install/helm-rancher/)



[The original source: mattiaperi/k3s-multipass-cluster](https://github.com/mattiaperi/k3s-multipass-cluster/blob/master/k3s-multipass-cluster.bash)

[Git repo: kubernetes-cluster-with-k3s-and-multipass](https://levelup.gitconnected.com/kubernetes-cluster-with-k3s-and-multipass-7532361affa3)

[Go to Top](#TopOfForm)

